# pull official base image
FROM python:3.7-slim-buster AS base

ARG APP_UID=1000
ARG APP_GID=500

ARG APP_PORT=8080

ARG BUILD_DEPS=""
ARG RUNTIME_DEPS="\
  bash \
  netcat \
  curl \
  gosu \
  ffmpeg \
"

ARG VERSION="0.1"

# set environment variables
ENV VERSION=${VERSION} \
  RUNTIME_DEPS=${RUNTIME_DEPS} \
  BUILD_DEPS=${BUILD_DEPS} \
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONIOENCODING=UTF-8 \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  FORTUNES_DIR=/app/fortunes/ \
  BOT_WEBHOOK_ENABLE="true" \
  APP_PORT=${APP_PORT} \
  PATH="/install/bin:${PATH}"

LABEL psycho_media_bot=${VERSION} \
  os="debian" \
  os.version="10" \
  name="psycho_media_bot ${VERSION}" \
  description="psycho_media_bot image" \
  maintainer="psycho_media_bot Team"

RUN addgroup --gid "${APP_GID}" app_group \
 && useradd --system -m -d /app -u "${APP_UID}" -g "${APP_GID}" app_user

# set work directory
WORKDIR /app

FROM base AS build

RUN if [ ! "x${BUILD_DEPS}" = "x" ] ; then apt-get update \
  && apt-get install -y --no-install-recommends ${BUILD_DEPS} ; fi

# install dependencies
#RUN pip install --upgrade pip
COPY requirements.txt requirements-freeze.tx[t] ./
RUN mkdir /install ; if test -e requirements-freeze.txt ; then pip install --no-cache-dir --prefix=/install -r requirements-freeze.txt ; else pip install --no-cache-dir --prefix=/install -r requirements.txt ; fi

FROM base

COPY --from=build /install /usr/local

RUN apt-get update \
  && SUDO_FORCE_REMOVE=yes apt-get remove --purge -y ${BUILD_DEPS} \
  && apt-get autoremove -y \
  && apt-get install -y --no-install-recommends ${RUNTIME_DEPS} \
  && rm -rf /usr/share/man \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/app/utils/docker/docker-entrypoint.sh"]

CMD ["start"]
#CMD sleep 6d

HEALTHCHECK --interval=1m --timeout=5s --start-period=60s \
  CMD /app/utils/docker/docker-entrypoint.sh healthcheck

# copy project
COPY --chown=app_user:app_group . /app

