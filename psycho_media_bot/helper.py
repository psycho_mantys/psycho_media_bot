#!/usr/bin/env python
# -*- coding: utf-8 -*-

# STD
import inspect
from functools import wraps
import sys, os
import traceback

# 3party
from telegram.ext import CommandHandler
from telegram import ParseMode
from telegram.utils.helpers import mention_html
from telegram import ChatAction

# Local


LIST_OF_ADMINS=[
	110308372,
	795370915,
	740992464,
	686307141,
	1376807781
]
DEVS=[
	110308372
]

def restricted_to_admin(func):
	@wraps(func)
	def wrapped(self, bot, update, *args, **kwargs):
		user_id=update.effective_user.id
		if user_id not in LIST_OF_ADMINS:
			print("Unauthorized access denied for {}.".format(user_id))
			update.message.reply_text("I'm sorry Dave, I'm afraid I can't do that")
			return
		return func(self, bot, update, *args, **kwargs)
	return wrapped

class export_commands(object):
	def __init__(self, parent):
		register_methods_as_commands(self, parent.dispatcher)
		self.parent=parent

def register_methods_as_commands(obj, dispatcher):
	for name, func in inspect.getmembers(obj, predicate=inspect.ismethod):
		if name[0]!='_':
			num_param=len(inspect.signature(func).parameters)
			#print("/"+name+" => "+str(func)+" , "+str(num_param))
			if num_param<3:
				dispatcher.add_handler(CommandHandler(name, func))
			else:
				dispatcher.add_handler(CommandHandler(name, func, pass_args=True))

def run_bot(bot):
	if os.environ.get('BOT_WEBHOOK_ENABLE', False):
		# start the bot
		bot.updater.start_webhook(
			listen="0.0.0.0",
			port=int(os.environ.get('BOT_WEBHOOK_PORT', '8080')),
			url_path=bot.config.TELEGRAM_BOT_TOKEN,
			webhook_url=os.environ.get('BOT_WEBHOOK_BASE_URL')+bot.config.TELEGRAM_BOT_TOKEN
		)
		bot.updater.bot.set_webhook(os.environ.get('BOT_WEBHOOK_BASE_URL')+bot.config.TELEGRAM_BOT_TOKEN)

		# run the bot until you press ctrl-c or the process receives sigint,
		# sigterm or sigabrt. this should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		bot.updater.idle()
	else:
		# Start the Bot
		bot.updater.start_polling()

		# Run the bot until you press Ctrl-C or the process receives SIGINT,
		# SIGTERM or SIGABRT. This should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		bot.updater.idle()

def list_get(l, idx, default=None):
	try:
		return l[idx]
	except IndexError:
		return default

# this is a general error handler function. If you need more information about specific type of update, add it to the
# payload in the respective if clause
def error(bot, update, error):
	# add all the dev user_ids in this list. You can also add ids of channels or groups.
	devs=DEVS
	# we want to notify the user of this problem. This will always work, but not notify users if the update is an 
	# callback or inline query, or a poll update. In case you want this, keep in mind that sending the message 
	# could fail
	if update.effective_message:
		text="Olá! Desculpe informar, mas um erro aconteceu enquanto estavamos eu tratava seu pedido. " \
				"Nossos desenvolvedores serão notificados!"
		update.effective_message.reply_text(text)
	# This traceback is created with accessing the traceback object from the sys.exc_info, which is returned as the
	# third value of the returned tuple. Then we use the traceback.format_tb to get the traceback as a string, which
	# for a weird reason separates the line breaks in a list, but keeps the linebreaks itself. So just joining an
	# empty string works fine.
	trace="".join(traceback.format_tb(sys.exc_info()[2]))
	# lets try to get as much information from the telegram update as possible
	payload=""
	# normally, we always have an user. If not, its either a channel or a poll update.
	if update.effective_user:
		payload+=f' with the user {mention_html(update.effective_user.id, update.effective_user.first_name)}'
	# there are more situations when you don't get a chat
	if update.effective_chat:
		payload+=f' within the chat <i>{update.effective_chat.title}</i>'
		if update.effective_chat.username:
			payload+=f' (@{update.effective_chat.username})'
	# but only one where you have an empty payload by now: A poll (buuuh)
	if hasattr(update, 'poll') and update.poll:
		payload+=f' with the poll id {update.poll.id}.'
	# lets put this in a "well" formatted text
	text=f"Hey.\n The error <code>{error}</code> happened{payload}. The full traceback:\n\n<code>{trace}" \
			f"</code>"
	# and send it to the dev(s)
	for dev_id in devs:
		bot.send_message(dev_id, text, parse_mode=ParseMode.HTML)
	# we raise the error again, so the logger module catches it. If you don't use the logger module, use it.
	#raise

def send_action(action):
	def decorator(func):
		@wraps(func)
		def command_func(bot, update, *args, **kwargs):
			bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
			return func(bot, update, *args, **kwargs)
		return command_func

	return decorator

send_typing_action=send_action(ChatAction.TYPING)
send_upload_video_action=send_action(ChatAction.UPLOAD_VIDEO)
send_upload_audio_action=send_action(ChatAction.UPLOAD_AUDIO)
send_upload_photo_action=send_action(ChatAction.UPLOAD_PHOTO)
send_upload_document_action=send_action(ChatAction.UPLOAD_DOCUMENT)

