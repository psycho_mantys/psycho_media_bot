#!/usr/bin/env python
# -*- coding: utf-8 -*-

# STD
from queue import Queue
import os
import pickle
from glob import glob
from functools import wraps

# 3party
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Dispatcher
from telegram.messageentity import MessageEntity
from telegram.error import NetworkError
from telegram.ext.dispatcher import run_async
from telegram import ChatAction
import yt_dlp as youtube_dl

# Local
from psycho_media_bot.commands import Commands
from psycho_media_bot.helper import list_get, restricted_to_admin
from psycho_media_bot import helper
from psycho_media_bot.async_help import Async_help


class Psycho_media_bot(object):
	def __init__(self, config, bot=None, dispatcher=None):
		self.config=config
		self.user=None

		#if os.path.isfile(config.TELEGRAM_BOT_USER_ID_FILE):
		#	with open(config.TELEGRAM_BOT_USER_ID_FILE, 'rb') as f:
		#		self.user=pickle.load(f)
		#else:
		#	self.user=dict()
		#	self.user["@psycho_mantys"]=110308372

		if bot:
			self.bot=bot
			self.update_queue=Queue()
			if not dispatcher:
				self.dispatcher=Dispatcher(bot, self.update_queue)
			else:
				self.dispatcher=dispatcher
			self.config.TELEGRAM_BOT_USERNAME=bot.username
		else:
			# Create the EventHandler and pass it your bot's token.
			#print("token set: {}".format(TELEGRAM_BOT_TOKEN))
			self.updater=Updater(self.config.TELEGRAM_BOT_TOKEN)
			self.bot=self.updater.bot
			# Get the dispatcher to register handlers
			self.dispatcher=self.updater.dispatcher
			self.config.TELEGRAM_BOT_USERNAME=self.updater.bot.username

		# log all errors
		self.dispatcher.add_error_handler(self.error)

		"""Bind commands(/somethings)"""
		self.commands=Commands(self)

		# any mention on start of message
		#self.dispatcher.add_handler(MessageHandler(
		#	Filters.text & Filters.entity(MessageEntity.MENTION), self.mention_roll)
		#)
		# message is not a group chat and text to bot
		#self.dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.group, self.dm_roll))

	def error(self, bot, update, error):
		"""Log Errors caused by Updates."""
		self.config.logger.warning('Update "%s" caused error "%s"', update, error)
		helper.error(bot, update, error)

	def start(self, bot, update):
		"""Send a message when the command /start is issued."""
		update.message.reply_text("""Bot para gerenciar meus desejos multimidias.""")

	def help(self, bot, update):
		update.message.reply_markdown("""
Bot para minhas funções multimidias

/dla - Video sem audio
/dl - Video com audio
/dla_low - Video sem audio com baixo bitrate e tamanho

Talvez de acordo com meu tempo eu tente fazer o inline e a referencia direta em grupos.
""")

	@Async_help()
	def download_media(self, bot, update, args, dl_opts={}, file_type='file'):
		chat_id=update.effective_chat.id
		user_id=update.message.from_user.id
		msg_id=update.message.message_id

		filename=' '.join(dl_opts['outtmpl'].split('.')[0:-1])
		self.config.logger.warning(f'Download {args[0]} for {filename}')

		if 'twitter.com' in args[0] and self.config.TWITTER_USERNAME is not None:
			dl_opts['username']=self.config.TWITTER_USERNAME
			dl_opts['password']=self.config.TWITTER_PASSWORD

		try:
			if file_type=='audio':
				bot.send_chat_action(
					chat_id=chat_id,
					action=ChatAction.RECORD_AUDIO
				)
			elif file_type=='video':
				bot.send_chat_action(
					chat_id=chat_id,
					action=ChatAction.RECORD_VIDEO
				)
			else:
				bot.send_chat_action(
					chat_id=chat_id,
					action=ChatAction.UPLOAD_DOCUMENT
				)

			with youtube_dl.YoutubeDL(dl_opts) as ydl:
				info=ydl.extract_info(args[0], download=True)

			filename=glob(filename+'*')[0]

			self.config.logger.warning(f'Sending {args[0]} of {filename}')

			with open(filename, 'rb') as f:
				if file_type=='audio':
					bot.send_chat_action(
						chat_id=chat_id,
						action=ChatAction.UPLOAD_AUDIO
					)
					update.message.reply_audio(
						quote=True,
						timeout=self.config.timeout,
						audio=f
					)
				elif file_type=='video':
					bot.send_chat_action(
						chat_id=chat_id,
						action=ChatAction.UPLOAD_VIDEO
					)
					update.message.reply_video(
						quote=True,
						video=f,
						timeout=self.config.timeout
					)
				else:
					bot.send_chat_action(
						chat_id=chat_id,
						action=ChatAction.UPLOAD_DOCUMENT
					)
					update.message.reply_document(
						quote=True,
						document=f,
						timeout=self.config.timeout
					)
		finally:
			os.remove(filename)

		self.config.logger.warning(f'Download {args[0]} for {filename} END!!')

	@restricted_to_admin
	def download_audio(self, bot, update, args):
		chat_id=update.effective_chat.id
		user_id=update.message.from_user.id
		msg_id=update.message.message_id

		filename=f'tmp/{user_id}-{chat_id}-{msg_id}'

		dl_opts={
			'format': 'bestaudio[ext=m4a]/m4a/best',
			'outtmpl': filename+'.%(ext)s',
			'noprogress': True,
			'noplaylist' : True,
			'postprocessors': [{
				'key': 'FFmpegExtractAudio',
				'preferredcodec': 'opus'
			}]
		}
		self.download_media(bot, update, args, dl_opts=dl_opts, file_type='audio')

	@restricted_to_admin
	def download_audio_low(self, bot, update, args):
		chat_id=update.effective_chat.id
		user_id=update.message.from_user.id
		msg_id=update.message.message_id

		filename=f'tmp/{user_id}-{chat_id}-{msg_id}'

		dl_opts={
			'format': 'bestaudio[ext=m4a]/m4a/bestaudio',
			'outtmpl': filename+'.%(ext)s',
			'noprogress': True,
			'noplaylist' : True,
			'postprocessors': [{
				'key': 'FFmpegExtractAudio',
				'preferredcodec': 'opus',
				'preferredquality': '32'
			}]
		}
		self.download_media(bot, update, args, dl_opts=dl_opts, file_type='audio')

	@restricted_to_admin
	def download_video(self, bot, update, args):
		chat_id=update.effective_chat.id
		user_id=update.message.from_user.id
		msg_id=update.message.message_id

		filename=f'tmp/{user_id}-{chat_id}-{msg_id}'

		dl_opts={
			'format': 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4',
			'outtmpl': filename+'.%(ext)s',
			'noprogress': True,
			'noplaylist' : True
		}
		self.download_media(bot, update, args, dl_opts=dl_opts, file_type='video')

