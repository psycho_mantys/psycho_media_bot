#!/usr/bin/env python
# -*- coding: utf-8 -*-

# STD

# 3party

# Local
from psycho_media_bot import helper

class Commands(helper.export_commands):
	def start(self, bot, update):
		self.parent.start(bot, update)

	def help(self, bot, update):
		self.parent.help(bot, update)

	def dla(self, bot, update, args=None):
		self.parent.download_audio(bot, update, args)

	def dla_low(self, bot, update, args=None):
		self.parent.download_audio_low(bot, update, args)

	def dl(self, bot, update, args=None):
		self.parent.download_video(bot, update, args)

