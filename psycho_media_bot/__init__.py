#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from telegram.ext import MessageHandler, Filters
from subprocess import Popen, PIPE
from optparse import OptionParser

from psycho_media_bot.config import Config
from psycho_media_bot.psycho_media_bot import Psycho_media_bot

from psycho_media_bot.helper import run_bot

def main():
	config=Config()

	config.load_cli()
	config.load_sys_env()

	"""Start the bot."""

	psycho_media_bot=create_from_bot(None, config)

	run_bot(psycho_media_bot)

def create_from_bot(bot, config=None):
	if not config:
		config=Config()

		config.load_cli()
		config.load_sys_env()

	config.logger.warning('INIT')

	"""Start the bot."""
	psycho_media_bot=Psycho_media_bot(config, bot)

	return psycho_media_bot

if __name__ == '__main__':
	main()
	
