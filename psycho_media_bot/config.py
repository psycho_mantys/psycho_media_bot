#!/usr/bin/env python
# -*- coding: utf-8 -*-

# STD
import logging
import argparse
import os
from os import environ

# 3party

# Local



class Config():
	def __init__(self):
		self.TELEGRAM_BOT_TOKEN=None
		self.TELEGRAM_BOT_USERNAME=None
		self.TWITTER_USERNAME=None
		self.TWITTER_PASSWORD=None
		self.timeout=999
		
		# Ativar debug se > 0
		self.DEBUG=0

		# Enable logging
		logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
			level=logging.INFO)

		self.logger=logging.getLogger(__name__)

	def load_cli(self):
		parser=argparse.ArgumentParser()

		parser.add_argument(
			"--debug","-d",
			nargs='?', const=1, default=self.DEBUG,
			metavar='LEVEL',
			dest='DEBUG',
			help="Enable debug level mode"
		)
		parser.add_argument(
			"-t", "--token-telegram",
			help="Token recived from FatherBot",
			type=str,
			default=self.TELEGRAM_BOT_TOKEN,
			metavar='TELEGRAM_BOT_TOKEN',
			dest='TELEGRAM_BOT_TOKEN'
		)
		parser.add_argument(
			"-u", "--username",
			help="Bot username if not provided",
			type=str,
			default=self.TELEGRAM_BOT_USERNAME,
			metavar='TELEGRAM_BOT_USERNAME',
			dest='TELEGRAM_BOT_USERNAME'
		)
		parser.add_argument(
			"--timeout",
			help="Bot send timeout",
			type=int,
			default=self.timeout,
			metavar='timeout',
			dest='timeout'
		)
		# Twitter
		parser.add_argument(
			'--twitter-username',
			help="Username for twitter",
			type=str,
			default=self.TWITTER_USERNAME,
			metavar='TWITTER_USERNAME',
			dest='TWITTER_USERNAME'
		)
		parser.add_argument(
			'--twitter-password',
			help="Password for twitter",
			type=str,
			default=self.TWITTER_PASSWORD,
			metavar='TWITTER_PASSWORD',
			dest='TWITTER_PASSWORD'
		)
		args=parser.parse_args()

		self.TELEGRAM_BOT_TOKEN=args.TELEGRAM_BOT_TOKEN
		self.TELEGRAM_BOT_USERNAME=args.TELEGRAM_BOT_USERNAME
		self.timeout=args.timeout

		# Enable debug
		self.DEBUG=args.DEBUG

	def load_sys_env(self):
		self.TELEGRAM_BOT_TOKEN=os.getenv('TELEGRAM_BOT_TOKEN', self.TELEGRAM_BOT_TOKEN)
		self.TELEGRAM_BOT_USERNAME=os.getenv('TELEGRAM_BOT_USERNAME', self.TELEGRAM_BOT_USERNAME)
		self.timeout=os.getenv('timeout', self.timeout)

		# Enable debug
		self.DEBUG=int(os.getenv('DEBUG', self.DEBUG))

