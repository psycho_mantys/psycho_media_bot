#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from psycho_media_bot import main, create_from_bot
import psycho_media_bot

def create_from_bot(*args, **kwargs):
	return psycho_media_bot.create_from_bot(*args, **kwargs)

if __name__ == '__main__':
	psycho_media_bot.main()

