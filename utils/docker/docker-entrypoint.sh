#!/bin/bash

set -e

do_gosu(){
	user="$1"
	shift 1

	is_exec="false"
	if [ "$1" = "exec" ] ; then
		is_exec="true"
		shift 1
	fi

	if [ "$(id -u)" = "0" ] ; then
		if [ "${is_exec}" = "true" ] ; then
			exec gosu "${user}" "$@"
		else
			gosu "${user}" "$@"
		fi
	else
		if [ "${is_exec}" = "true" ] ; then
			exec "$@"
		else
			eval "$@"
		fi
	fi
}

parse_env(){
	if [ -r "$1" ] ; then
		while IFS="=" read key value  ; do
			export "${key}=${value}"
		done<<<"$( egrep '^[^#]+=.*' "$1" )"
	fi
}

bootstrap_conf(){
	if [ "x${DO_CHOWN}" = "xtrue" ] ; then
		find /app -not -user app_user -exec chown app_user:app_group {} \+
	fi
}

parse_env '/env.sh'
parse_env '/run/secrets/env.sh'

bootstrap_conf

if [[ "start" == "$1" ]]; then
	do_gosu app_user:app_group exec ./main.py
elif [[ "healthcheck-port" == "$1" ]]; then
	do_gosu app_user:app_group exec nc -z -w5 127.0.0.1 "${2}"
elif [[ "healthcheck" == "$1" ]]; then
	exec nc -z -w5 127.0.0.1 "${APP_PORT}"
fi

exec "$@"

